﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace AWS.Services.Interfaces
{
    public interface IBaseService
    {
        public IConfiguration Configuration { get; }
        public Task<HttpResponseMessage> Post(string uri, StringContent data, string token);


    }
}
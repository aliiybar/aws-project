﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AWS.Models;
using Microsoft.Extensions.Configuration;

namespace AWS.Services.Interfaces
{
    public interface IIssueService
    {
        Task<List<IssueDto>> GetIssues(string token);
        IConfiguration Configuration { get; }
      
    }
}
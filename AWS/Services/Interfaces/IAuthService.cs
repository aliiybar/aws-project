﻿using System.Threading.Tasks;
using AWS.Models;
using Microsoft.Extensions.Configuration;

namespace AWS.Services.Interfaces
{
    public interface IAuthService
    {
        Task<LoginResult> Login(string username, string password);
        IConfiguration Configuration { get; }
    }
}
﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AWS.Services.Interfaces;
using Microsoft.Extensions.Configuration;

namespace AWS.Services.Implementations
{
    public class BaseService : IBaseService
    {
        public IConfiguration Configuration { get; }
        public string apiUri;
        public string apiPath;
        public static readonly HttpClient client = new HttpClient();

        public BaseService(IConfiguration configuration)
        {
            Configuration = configuration;
            apiUri = Configuration.GetSection("GitLab:Uri").Value;
            apiPath = Configuration.GetSection("GitLab:ApiPath").Value;
        }

        public async Task<HttpResponseMessage> Post(string uri, StringContent data, string token)
        {
            if (token != null)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            return await client.PostAsync(uri, data);
        }

        public async Task<HttpResponseMessage> Get(string uri, StringContent data, string token)
        {
            if (token != null)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            return await client.GetAsync(uri);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AWS.Models;
using AWS.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;

namespace AWS.Services.Implementations
{
    public class IssueService:BaseService, IIssueService
    {
        public IssueService(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<List<IssueDto>> GetIssues( string token)
        {
            if (token != null)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            var IssueResult = await client.GetAsync(apiUri + apiPath + "issues" );
            var Issues =  JsonConvert.DeserializeObject<List<Issue>>(IssueResult.Content.ReadAsStringAsync().Result);
            return Issues.Select(a => new IssueDto(a)).ToList();
        }
    }
}

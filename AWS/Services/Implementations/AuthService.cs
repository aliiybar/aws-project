﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AWS.Models;
using AWS.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace AWS.Services.Implementations
{
    public class AuthService :BaseService, IAuthService
    {
        public AuthService(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<LoginResult> Login(string username, string password)
        {
            var login = new Login(){Username = username, Password = password};
            var json = JsonConvert.SerializeObject(login);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
           
            var loginResult =  await Post(apiUri + "oauth/token", data, null);
            return loginResult.StatusCode == HttpStatusCode.OK ? 
                JsonConvert.DeserializeObject<LoginResult>(loginResult.Content.ReadAsStringAsync().Result) : 
                null;
        }
    }
}

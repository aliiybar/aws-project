﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AWS.Models;
using AWS.Services;
using AWS.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AWS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;


        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }
        [HttpPost]
        public async Task<IActionResult> Post()
        {
            return Ok("OK");
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok("OK");
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromForm] string  username, [FromForm] string password)
        {
            if (!ModelState.IsValid) return BadRequest("Wrong parameters");
                
                var loginResult = await _authService.Login(username, password);
           return Ok(loginResult);
        }
    }
}

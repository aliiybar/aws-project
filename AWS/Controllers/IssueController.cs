﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AWS.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AWS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IssueController : ControllerBase
    {
        private readonly IIssueService _issueService;

        public IssueController(IIssueService issueService)
        {
            _issueService = issueService;
        }
        [HttpGet("Get")]
        public async Task<IActionResult> Get( [FromForm] string token)
        {
            var result = await _issueService.GetIssues(token);
            return Ok(result);
        }
    }
}

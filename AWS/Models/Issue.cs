﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AWS.Models
{
    public class Issue
    {
        public string Title { get; set; }
        public string Description { get; set; }
        [JsonProperty("state")]
        public string Status { get; set; }
         public Author Author { get; set; }
         public Assignee Assignee { get; set; }
         public Milestone Milestone { get; set; }
         public string[] Labels { get; set; }
        [JsonProperty("created_at")]
        public DateTime DateCreated { get; set; }
        [JsonProperty("updated_at")]
        public DateTime? DateUpdated { get; set; }
        [JsonProperty("closed_at")]
        public DateTime? DateClosed { get; set; }
        [JsonProperty("due_date")]
        public DateTime? DateDue { get; set; }

      
    }
}

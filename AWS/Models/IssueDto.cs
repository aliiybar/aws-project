﻿using System;

namespace AWS.Models
{
    public class IssueDto
    {
        public IssueDto(Issue issue)
        {
            Title = issue.Title;
            Description = issue.Description;
            Status = issue.Status;
            AuthorName = issue.Author.Name;
            Assignee = issue.Assignee?.Name;
            Milestone = issue.Milestone?.Description;
            Labels = issue.Labels;
            DateCreated = issue.DateCreated;
            DateClosed = issue.DateClosed;
            DateUpdated = issue.DateUpdated;
            DateDue = issue.DateDue;

        }
        public string Title { get; set; }
        public string Description  { get; set; }
        public string Status { get; set; }
        public string AuthorName  { get; set; }
        public string Assignee { get; set; }
        public string Milestone { get; set; }
        public string[]  Labels { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateClosed { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDue { get; set; }
    }
}
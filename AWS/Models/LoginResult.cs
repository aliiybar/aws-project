﻿using Newtonsoft.Json;

namespace AWS.Models
{
    public class LoginResult
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

    }
}